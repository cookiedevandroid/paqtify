package data.net

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by artCore on 9/18/17.
 */
data class NetErrorModel(
    @SerializedName("message") val msg: String,
    //@SerializedName("dev_message") val dev_msg: String,
    @SerializedName("code") val code: Int = -1
): Serializable