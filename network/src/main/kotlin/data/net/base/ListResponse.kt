package data.net.base

import com.google.gson.annotations.SerializedName

class ListResponse<out T> {
    @SerializedName("list") private val _list: List<T> ? = emptyList()
    @SerializedName("total_count") val totalCount: Int = 0
    @SerializedName("offset") val offset: Int = 0
    val list: List<T>
        get() = _list ?: emptyList()
}