package network.core.experimental

import java.io.Serializable

/**
 * Created by artCore on 02.08.19.
 */
interface ErrorDetails : Serializable

    class BadRequest(
        val reason: String, // <VALIDATION_RULE_BROKEN> or <MUTUAL_VALIDATION_INCONSISTENCY>,
        val message: String // <usually description of which validation requirement was not meet>
    ) : ErrorDetails {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is BadRequest) return false

            if (reason != other.reason) return false
            if (message != other.message) return false

            return true
        }

        override fun hashCode(): Int {
            var result = reason.hashCode()
            result = 31 * result + message.hashCode()
            return result
        }

        override fun toString(): String {
            return "BadRequest(reason='$reason', message='$message')"
        }
    }

class Unauthorized(
    val reason: String, // <NO_USER> or <BAD_CREDENTIALS> or <CREDENTIALS_EXPIRED> or <NO_TOKEN> or <INTERNAL_ISSUE> or <OTHER>,
    val message: String // <usually description of why Authentication has failed>
) : ErrorDetails {

    fun getType(): Type? {
        return Type.fromReason(reason)
    }

    companion object {
        @JvmOverloads
        fun create(type: Type = Type.CREDENTIALS_EXPIRED, message: String = "") =
            Unauthorized(type.name, message)
    }

    enum class Type {
        NO_USER,
        BAD_CREDENTIALS,
        CREDENTIALS_EXPIRED,
        NO_TOKEN,
        INTERNAL_ISSUE,
        OTHER;

        companion object {
            fun fromReason(reason: String) = values().firstOrNull { reason.toUpperCase() == it.name }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Unauthorized) return false

        if (reason != other.reason) return false
        if (message != other.message) return false

        return true
    }

    override fun hashCode(): Int {
        var result = reason.hashCode()
        result = 31 * result + message.hashCode()
        return result
    }

    override fun toString(): String {
        return "Unauthorized(reason='$reason', message='$message')"
    }
}

class Forbidden : ErrorDetails {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Forbidden) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

    override fun toString(): String {
        return "Forbidden()"
    }
}

class NotFound(
    val entityType: String, // <Entity type which has not been found, e.g. "User">,
    val identityType: String, // <Identity type which has been used for searching the Entity, e.g. "ID" or "Email">,
    val identityValue: String, // <Identity value which has been used for searching the Entity, e.g. "25" or "test@test.com">,
    val reason: String, // "NOT_FOUND",
    val message: String // <usually description of that the Entity has not been found>
) : ErrorDetails {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is NotFound) return false

        if (entityType != other.entityType) return false
        if (identityType != other.identityType) return false
        if (identityValue != other.identityValue) return false
        if (reason != other.reason) return false
        if (message != other.message) return false

        return true
    }

    override fun hashCode(): Int {
        var result = entityType.hashCode()
        result = 31 * result + identityType.hashCode()
        result = 31 * result + identityValue.hashCode()
        result = 31 * result + reason.hashCode()
        result = 31 * result + message.hashCode()
        return result
    }

    override fun toString(): String {
        return "NotFound(entityType='$entityType', identityType='$identityType', identityValue='$identityValue', reason='$reason', message='$message')"
    }
}

class Conflict(
    val entityType: String, // <Entity type which exists already, e.g. "User">,
    val identityType: String, // <Identity type which has been used to create the Entity, e.g. "ID" or "Email">,
    val identityValue: String, // <Identity value which has been used to create the Entity, e.g. "25" or "test@test.com">,
    val reason: String, // "EXISTS_ALREADY",
    val message: String// <usually description of that the Entity exists already>
) : ErrorDetails {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Conflict) return false

        if (entityType != other.entityType) return false
        if (identityType != other.identityType) return false
        if (identityValue != other.identityValue) return false
        if (reason != other.reason) return false
        if (message != other.message) return false

        return true
    }

    override fun hashCode(): Int {
        var result = entityType.hashCode()
        result = 31 * result + identityType.hashCode()
        result = 31 * result + identityValue.hashCode()
        result = 31 * result + reason.hashCode()
        result = 31 * result + message.hashCode()
        return result
    }

    override fun toString(): String {
        return "Conflict(entityType='$entityType', identityType='$identityType', identityValue='$identityValue', reason='$reason', message='$message')"
    }
}

class InternalServerError(
    val externalFailureCategory: String, // <CLIENT> or <SERVER> or <OTHER>,
    val statusCode: Int, // <HTTP status code which has been produced by external service>,
    val reason: String, // <INTERNAL_SERVICE_ERROR> or <EXTERNAL_SERVICE_ERROR>,
    val message: String // <usually general description of what happened>
): ErrorDetails {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is InternalServerError) return false

        if (externalFailureCategory != other.externalFailureCategory) return false
        if (statusCode != other.statusCode) return false
        if (reason != other.reason) return false
        if (message != other.message) return false

        return true
    }

    override fun hashCode(): Int {
        var result = externalFailureCategory.hashCode()
        result = 31 * result + statusCode
        result = 31 * result + reason.hashCode()
        result = 31 * result + message.hashCode()
        return result
    }

    override fun toString(): String {
        return "InternalServerError(externalFailureCategory='$externalFailureCategory', statusCode=$statusCode, reason='$reason', message='$message')"
    }
}

class Unknown: ErrorDetails {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Unknown) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

    override fun toString(): String {
        return "Unknown()"
    }
}