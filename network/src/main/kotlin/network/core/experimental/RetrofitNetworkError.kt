package network.core.experimental

import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException

/**
 * Created by Ivan on 10.08.16.
 */
sealed class RetrofitNetworkError constructor(
    message: String?,
    exception: Throwable?
) : RuntimeException(message, exception) {

    abstract override fun toString(): String

    class Http (
        message: String?,
        /** The request URL which produced the error.  */
        val url: String?,
        /** The Retrofit this request was executed on  */
        val details: ErrorDetails? = null
    ) : RetrofitNetworkError(message, null) {

        override fun toString(): String {
            return "Http(url=$url, details=$details)"
        }
    }

    class NetworkConnection(exception: Throwable?): RetrofitNetworkError(exception?.message, exception) {
        override fun toString(): String {
            return "NetworkConnection(message=$message, cause=$cause)"
        }
    }

    class Conversion(exception: Throwable?) : RetrofitNetworkError(exception?.message, exception) {
        override fun toString(): String {
            return "Conversion(message=$message, cause=$cause)"
        }
    }

    class Unexpected(throwable: Throwable?): RetrofitNetworkError(throwable?.message, throwable) {
        override fun toString(): String {
            return "Unexpected(message=$message, cause=$cause)"
        }
    }

    companion object {
        @Throws(IOException::class)
        fun httpError(url: String, response: Response<*>?, retrofit: Retrofit): RetrofitNetworkError {
            val message: String = if (response != null) {
                "${response.code()}" + " " + response.message()
            } else
                "unknown error"

            val details = ServerErrorMapper().map(retrofit, response)
            return Http(message, url, details)
        }

        fun connectionError(exception: IOException): RetrofitNetworkError =
            NetworkConnection(exception)

        fun parsingError(exception: Throwable): RetrofitNetworkError =
            Conversion(exception)

        fun unexpectedError(exception: Throwable): RetrofitNetworkError =
            Unexpected(exception)
    }
}

