package network.core.experimental

import com.google.gson.JsonSyntaxException
import retrofit2.Response
import retrofit2.Retrofit
import java.io.EOFException

/**
 * Created by artCore on 05.08.19.
 */
class ServerErrorMapper  {
    fun map (retrofit: Retrofit, response: Response<*>?): ErrorDetails? {
        return when(response?.code() ?: -1) {
            400 -> convertError<BadRequest>(retrofit, response)
            401 -> convertError<Unauthorized>(retrofit, response)
            403 -> convertError<Forbidden>(retrofit, response)
            404 -> convertError<NotFound>(retrofit, response)
            409 -> convertError<Conflict>(retrofit, response)
            500 -> convertError<InternalServerError>(retrofit, response)
            else -> convertError<Unknown>(retrofit, response)
        }
    }

    private inline fun <reified T: ErrorDetails> convertError(retrofit: Retrofit, response: Response<*>?): T? {
        val errorBody = response?.errorBody()
        return if (errorBody == null) {
            null
        } else {
            val converter = retrofit.responseBodyConverter<T>(T::class.java, arrayOfNulls(0))
            try {
                converter.convert(errorBody)  as T
            } catch (ex: Exception) {
                ex.printStackTrace()
                when (ex) {
                    !is EOFException, !is JsonSyntaxException -> ex.printStackTrace()
                    else -> throw ex
                }
                null
            }
        }
    }
}
