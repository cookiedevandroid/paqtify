package com.paqtify.app.data.common

/**
 * Created by artCore on 27.08.19.
 */
interface Mapper<T, R> where  T : Any?, R: Any ? {
    fun map(input: T): R
}