package com.paqtify.app.utils.extentions

import java.util.*

fun Double.format(digits: Int) = "%.${digits}f".format(Locale.US, this)