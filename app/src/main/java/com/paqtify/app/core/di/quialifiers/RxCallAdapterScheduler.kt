package com.mealshift.app.core.di.quialifiers

import javax.inject.Qualifier

@MustBeDocumented
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class RxCallAdapterScheduler