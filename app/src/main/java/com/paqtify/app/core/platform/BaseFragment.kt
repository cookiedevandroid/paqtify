package com.paqtify.app.core.platform

import android.os.Bundle
import android.view.View
import com.paqtify.app.utils.extentions.screenslog
import com.paqtify.app.R
import core.exception.Failure
import core.experimental.Error
import core.experimental.LiveDataResourceReadyView
import core.platform.BaseCoreFragment
import extensions.platform.hideAnim
import extensions.platform.showAnim

/**
 * Created by artCore on 30.07.19.
 */
abstract class BaseFragment : BaseCoreFragment(), LiveDataResourceReadyView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        screenslog(this.javaClass.canonicalName ?: "null")
    }

    override fun showProgress(requestId: Int?) {
        view?.findViewById<View>(R.id.progress)
            ?.showAnim()
    }

    override fun hideProgress(requestId: Int?) {
        view?.findViewById<View>(R.id.progress)
            ?.hideAnim()
    }

    override fun handleError(t: Throwable?) {
        (activity as? LiveDataResourceReadyView)?.handleError(t)
    }

    override fun <T> handleError(error: Error<T>) {
        (activity as? LiveDataResourceReadyView)?.handleError(error)
    }

    override fun handleFailure(failure: Failure) {
        (activity as? LiveDataResourceReadyView)?.handleFailure(failure)
    }
}
