package com.mealshift.client.core.di.quialifiers

import javax.inject.Qualifier

@MustBeDocumented
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class InterceptorLogTag