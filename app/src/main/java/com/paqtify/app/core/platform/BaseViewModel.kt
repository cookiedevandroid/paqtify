package com.paqtify.app.core.platform

import com.paqtify.app.domain.feature.ThrowableToFailureConverter
import core.platform.BaseCoreViewModel
import javax.inject.Inject

/**
 * This class just to add some project depend functionality.
 * You don't want to have code of this class in your baseModule.
 */
abstract class BaseViewModel : BaseCoreViewModel() {
    // add some functions for your custom project
    @Inject lateinit var errorConverter: ThrowableToFailureConverter
}
