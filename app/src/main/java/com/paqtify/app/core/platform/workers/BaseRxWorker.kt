package com.paqtify.app.core.platform.workers

import android.content.Context
import androidx.work.Constraints
import androidx.work.Data
import androidx.work.ExistingWorkPolicy
import androidx.work.NetworkType.CONNECTED
import androidx.work.OneTimeWorkRequest
import androidx.work.PeriodicWorkRequest
import androidx.work.RxWorker
import androidx.work.WorkInfo
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit

abstract class BaseRxWorker(
    appContext: Context, workerParams: WorkerParameters
) : RxWorker(appContext, workerParams) {

    companion object {
        inline fun <reified T : RxWorker> startWork(tag: String, inputData: Data? = null) {
            getManagerInstance().apply {
                cancelAllWorkByTag(tag)
                enqueue(getOneTimeWorkRequest<T>(tag, inputData))
            }
        }

        inline fun <reified T : RxWorker> uniqueStartWork(
            uniqueName: String, policy: ExistingWorkPolicy = ExistingWorkPolicy.KEEP, tag: String,
            inputData: Data? = null
        ) {
            getManagerInstance().apply {
                enqueueUniqueWork(uniqueName, policy, getOneTimeWorkRequest<T>(tag, inputData))
            }
        }

        inline fun <reified T : RxWorker> startPeriodicWork(
            tag: String, inputData: Data? = null, repeatInterval: Long,
            repeatIntervalTimeUnit: TimeUnit
        ) {
            getManagerInstance().apply {
                cancelAllWorkByTag(tag)
                enqueue(
                    getPeriodicWorkRequest<T>(tag,
                    inputData,
                    repeatInterval,
                    repeatIntervalTimeUnit)
                )
            }
        }

        fun stopWork(tag: String) = getManagerInstance().cancelAllWorkByTag(tag)

        fun isScheduled(tag: String): Boolean {
            val statuses = getManagerInstance().getWorkInfosByTag(tag)
            return try {
                var running = false
                val workInfoList = statuses.get()
                for (workInfo in workInfoList) {
                    val state = workInfo.state
                    running = (state == WorkInfo.State.RUNNING) or (state == WorkInfo.State.ENQUEUED)
                }
                running
            } catch (e: ExecutionException) {
                e.printStackTrace()
                false
            } catch (e: InterruptedException) {
                e.printStackTrace()
                false
            }
        }

        fun getManagerInstance() = WorkManager.getInstance()

        inline fun <reified T : RxWorker> getOneTimeWorkRequest(
            tag: String, inputData: Data? = null
        ): OneTimeWorkRequest {
            return getWorkRequestBuilder<T>(tag, inputData).build()
        }

        inline fun <reified T : RxWorker> getWorkRequestBuilder(
            tag: String, inputData: Data?
        ): OneTimeWorkRequest.Builder {
            val builder = OneTimeWorkRequest.Builder(T::class.java)
                .addTag(tag)
                .setConstraints(getConstraints())
            inputData?.let { builder.setInputData(it) }
            return builder
        }

        inline fun <reified T : RxWorker> getPeriodicWorkRequest(
            tag: String, inputData: Data? = null, repeatInterval: Long,
            repeatIntervalTimeUnit: TimeUnit
        ): PeriodicWorkRequest {
            return getPeriodicWorkRequestBuilder<T>(tag,
                inputData,
                repeatInterval,
                repeatIntervalTimeUnit).build()
        }

        inline fun <reified T : RxWorker> getPeriodicWorkRequestBuilder(
            tag: String, inputData: Data?, repeatInterval: Long, repeatIntervalTimeUnit: TimeUnit
        ): PeriodicWorkRequest.Builder {
            val builder = PeriodicWorkRequest.Builder(T::class.java,
                repeatInterval,
                repeatIntervalTimeUnit)
                .addTag(tag)
                .setConstraints(getConstraints())
            inputData?.let { builder.setInputData(it) }
            return builder
        }

        fun getConstraints(): Constraints {
            return Constraints.Builder()
                .setRequiredNetworkType(CONNECTED)
                .build()
        }
    }
}