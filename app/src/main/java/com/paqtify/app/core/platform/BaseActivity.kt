package com.paqtify.app.core.platform

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.annotation.StringRes
import com.paqtify.app.R
import com.paqtify.app.utils.extentions.isLogsEnabled
import core.exception.Failure
import core.exception.Failure.ValidationFailure
import core.experimental.Error
import core.experimental.LiveDataResourceReadyView
import core.platform.BaseCoreActivity
import extensions.platform.*

/**
 * Created by artCore on 30.07.19.
 */
abstract class BaseActivity : BaseCoreActivity(), LiveDataResourceReadyView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (shouldOverridePendingTransition()) {
            enterPendingTransition()
        }
    }

    override fun finish() {
        super.finish()

        if (shouldOverridePendingTransition()) {
            exitPendingTransition()
        }
    }

    override fun showProgress(requestId: Int?) {
        findViewById<View>(R.id.progress)?.showAnim()
    }

    override fun hideProgress(requestId: Int?) {
        findViewById<View>(R.id.progress)?.hideAnim()
    }

    override fun <T> handleError(error: Error<T>) {
        handleError(error.throwable)
    }

    private fun handleValidationFailure(failure: ValidationFailure) {
        val messageResource = when (failure) {
            else -> R.string.error_unknown
        }
        renderError(messageResource)
    }

    open fun shouldOverridePendingTransition() = true
    open fun enterPendingTransition() = overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    open fun exitPendingTransition() = overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

    override fun handleError(t: Throwable?) {
        if (isLogsEnabled()) {
            t?.printStackTrace()
        }
        // todo
    }

    override fun handleFailure(failure: Failure) {
        when (failure) {
            else -> {
                renderError(R.string.error_unknown)
            }
        }
    }

    fun setAdjustNothing() {
        window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)
    }

    fun setAdjustResize() {
        window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }

    @Suppress("MemberVisibilityCanBePrivate")
    protected fun renderError(
        @StringRes resource: Int,
        @StringRes titleRes: Int = R.string.error_title
    ) = Unit
}