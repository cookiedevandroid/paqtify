package com.paqtify.app.core.di

import android.content.Context
import android.util.Log
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.mealshift.client.core.di.quialifiers.InterceptorLogTag
import com.mealshift.app.core.di.quialifiers.RxCallAdapterScheduler
import com.paqtify.app.BuildConfig
import com.paqtify.app.utils.extentions.isLogsEnabled
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import network.core.experimental.RxErrorHandlingCallAdapterFactory
import okhttp3.MediaType
import okhttp3.OkHttpClient
import retrofit2.CallAdapter
import retrofit2.Retrofit
import utils.NETWORK
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class NetworkModule {

    @UnstableDefault
    @Singleton
    @Provides
    internal fun createRetrofit(
        okHttpClient: OkHttpClient,
        callAdapterFactory: CallAdapter.Factory
    ): Retrofit {
        val builder = Retrofit.Builder()
            .baseUrl(BuildConfig.ENDPOINT)
            .addConverterFactory(
                Json(
                    JsonConfiguration(
                        encodeDefaults = true,
                        ignoreUnknownKeys = true,
                        isLenient = false,
                        serializeSpecialFloatingPointValues = false,
                        allowStructuredMapKeys = true,
                        prettyPrint = false,
                        unquotedPrint = false,
                        useArrayPolymorphism = false
                    )
                ).asConverterFactory(MediaType.parse("application/json")!!)
            )
            .addCallAdapterFactory(callAdapterFactory)

        builder.client(okHttpClient)

        return builder.build()
    }

    @Provides
    internal fun errorHandleAdapterFactory(@RxCallAdapterScheduler scheduler: Scheduler): CallAdapter.Factory =
        RxErrorHandlingCallAdapterFactory.createWithScheduler(scheduler)

    @Provides
    @RxCallAdapterScheduler
    internal fun callFactoryScheduler(): Scheduler = Schedulers.io()

    @Singleton
    @Provides
    @InterceptorLogTag
    internal fun logTag(): String = NETWORK

    @Provides
    internal fun chuckInterceptor(@ApplicationContext context: Context): ChuckerInterceptor =
        ChuckerInterceptor(context)

    @Provides
    internal fun loggingInterceptor(@InterceptorLogTag logTag: String): LoggingInterceptor {
        return LoggingInterceptor.Builder()
            .loggable(isLogsEnabled())
            .setLevel(if (isLogsEnabled()) Level.BASIC else Level.NONE)
            .log(Log.INFO)
            .request(logTag)
            .response(logTag)
            .build()
    }

    @Provides
    internal fun okHttpClient(
        logging: LoggingInterceptor,
        chuck: ChuckerInterceptor
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .addInterceptor(logging)
            .addInterceptor(chuck)
            .readTimeout(TIME_OUT, TimeUnit.SECONDS)
            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(TIME_OUT, TimeUnit.SECONDS)

        return builder.build()
    }

    companion object {
        private const val TIME_OUT = 30L
    }
}