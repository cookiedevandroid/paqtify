package com.paqtify.app.core.platform

import android.os.Bundle
import com.paqtify.app.utils.extentions.screenslog
import core.platform.BaseCoreFragmentDialog

/**
 * Created by artCore on 11.10.19.
 */
abstract class BaseFragmentDialog: BaseCoreFragmentDialog() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        screenslog(this.javaClass.canonicalName ?: "null")
    }

    override fun onResume() {
        super.onResume()
        adjustDialogSize()
    }
}
