package com.paqtify.app.presentation.auth.bankid

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.paqtify.app.core.platform.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BankIdAuthFragment : BaseFragment() {
    override val layoutRes: Int = com.paqtify.app.R.layout.fragment_bank_id_auth
    private val viewModel: BankIdAuthViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUi()
    }

    private fun initUi() {
        //todo
    }
}