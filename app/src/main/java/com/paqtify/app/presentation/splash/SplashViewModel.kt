package com.paqtify.app.presentation.splash

import androidx.hilt.lifecycle.ViewModelInject
import com.paqtify.app.core.platform.BaseViewModel
import core.experimental.SingleLiveEvent
import io.reactivex.rxjava3.core.Observable
import java.util.concurrent.TimeUnit

class SplashViewModel @ViewModelInject constructor(): BaseViewModel() {
    private val _goToApp = SingleLiveEvent<Boolean>()
    fun goToApp() = _goToApp

    init {
        startTimer()
    }

    private fun startTimer() = autoDisposable {
        Observable
            .timer(DELAY, TimeUnit.SECONDS)
            .subscribe {
                _goToApp.postValue(true)
            }
    }

    companion object {
        private const val DELAY = 2L
    }
}