package com.paqtify.app.presentation.splash

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.paqtify.app.R
import com.paqtify.app.core.platform.BaseFragment
import core.exception.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashFragment : BaseFragment() {
    override val layoutRes: Int = R.layout.fragment_splash
    private val viewModel: SplashViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUi()
    }

    private fun initUi() {
        initViewModel()
    }

    private fun initViewModel() = with(viewModel) {
        observe(goToApp()) {
            it?.let {
                findNavController().navigate(R.id.action_splashFragment_to_bankIdAuthFragment)
            }
        }
    }
}