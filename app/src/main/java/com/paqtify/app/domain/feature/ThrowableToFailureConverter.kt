package com.paqtify.app.domain.feature

import com.paqtify.app.data.common.Mapper
import com.paqtify.app.utils.extentions.isLogsEnabled
import core.exception.Failure
import network.core.experimental.RetrofitNetworkError

class ThrowableToFailureConverter : Mapper<Throwable?, Failure> {
    override fun map(input: Throwable?): Failure {
        if (isLogsEnabled()) input?.printStackTrace()
        return when (input) {
            else -> {
                val details = if (input is RetrofitNetworkError.Http) input.details else null
                HttpFailure(details, input?.message)
            }
        }
    }
}