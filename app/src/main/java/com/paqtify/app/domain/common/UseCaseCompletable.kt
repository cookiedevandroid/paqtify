package com.paqtify.app.domain.common

import io.reactivex.Completable

abstract class UseCaseCompletable<in Params> : UseCase<Completable, Params>() {

    abstract override fun run(params: Params): Completable
}