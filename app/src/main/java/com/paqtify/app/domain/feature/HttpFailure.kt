package com.paqtify.app.domain.feature

import core.exception.Failure
import network.core.experimental.ErrorDetails

class HttpFailure(val details: ErrorDetails?, val message: String?) : Failure.RemoteFailure()