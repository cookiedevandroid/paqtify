package com.paqtify.app

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.util.Log
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.LogStrategy
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PaqtifyApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initLogger()
        registerLifecycleCallbacks(this)
    }

    private fun registerLifecycleCallbacks(app: Application) {
        val listener = object : ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) = Unit
            override fun onActivityStarted(activity: Activity) = Unit
            override fun onActivityResumed(activity: Activity) = Unit
            override fun onActivityPaused(activity: Activity) = Unit
            override fun onActivityStopped(activity: Activity) = Unit
            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) = Unit
            override fun onActivityDestroyed(activity: Activity) = Unit
        }
        app.registerActivityLifecycleCallbacks(listener)
    }

    private fun initLogger() {
        val prefix = Array(2) { ". " }
        prefix[1] = " ."
        var index = 0
        val logStrategy = LogStrategy { priority, tag, message ->
            index = index xor 1
            Log.println(priority, prefix[index] + tag, message)
        }

        val formatStrategy = PrettyFormatStrategy.newBuilder()
            .showThreadInfo(true)  // (Optional) Whether to show thread info or not. Default true
            .methodCount(6)         // (Optional) How many method line to show. Default 2
            .methodOffset(0)        // (Optional) Hides internal method calls up to offset. Default 5
            .logStrategy(logStrategy) // (Optional) Changes the log strategy to print out. Default LogCat
            .tag(LOGGER)   // (Optional) Global tag for every log. Default PRETTY_LOGGER
            .build()

        Logger.addLogAdapter(AndroidLogAdapter(formatStrategy))
    }

    companion object {
        private const val LOGGER = "PrettyLogger"
    }
}
