package utils

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import com.base.module.R
import extensions.platform.message
import extensions.platform.okButton
import extensions.platform.showAlert
import extensions.platform.title

object EMailSender {

    fun send(activity: Activity, email: CharSequence) =
        send(activity, Params(arrayOf(email.toString())))

    fun send(activity: Activity, params: Params) {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:")
            with(params) {
                putExtra(Intent.EXTRA_EMAIL, mailtoAddress)

                extraSubject?.let {
                    putExtra(Intent.EXTRA_SUBJECT, extraSubject)
                }
                body?.let {
                    putExtra(Intent.EXTRA_TEXT, params.body)
                }

                if (!attachmentLocation.isNullOrBlank()) {
                    putExtra(Intent.EXTRA_STREAM, Uri.parse("file://$attachmentLocation"))
                }
            }
        }

        try {
            activity.startActivity(Intent.createChooser(intent, ""))
        } catch (e: ActivityNotFoundException) {
            activity.showAlert {
                title(activity.getString(R.string.error_header))
                message(activity.getString(R.string.mailto_not_found))
                okButton { }
            }
        }
    }

    class Params(
        val mailtoAddress: Array<String>, val extraSubject: String? = null,
        val body: String? = null, val attachmentLocation: String? = null
    )
}