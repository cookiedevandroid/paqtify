package extensions.platform

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by artCore on 11/30/17.
 */
const val ISO_8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
const val ISO_8601_FORMAT_NO_Z = "yyyy-MM-dd'T'HH:mm:ss"

enum class Lang { FRA, ENG }

fun Date.formatted(pattern: String = ISO_8601_FORMAT, locale : Locale = Locale.getDefault(), timeZone: TimeZone = TimeZone.getDefault()): String {
  val format = SimpleDateFormat(pattern, locale).apply { this.timeZone = timeZone }
  return format.format(this)
}

fun Date.toCalendar(): Calendar {
  val calendar = Calendar.getInstance()
  calendar.time = this
  return calendar
}

@Throws(java.text.ParseException::class)
fun dateFrom(
    string: String,
    pattern: String = ISO_8601_FORMAT,
    locale: Locale = Locale.ENGLISH,
    timeZone: TimeZone = TimeZone.getTimeZone("UTC"),
    retryWith: (() -> Date?)? = null
): Date? {
    val format = SimpleDateFormat(pattern, locale)
    format.timeZone = timeZone
    return try {
        format.parse(string)
    } catch (ex: java.text.ParseException) {
        return retryWith?.invoke()
    }
}

 fun Date.truncateToDay(timeZone: TimeZone): Date {
  val calendar = Calendar.getInstance(timeZone)
  calendar.time = this
  calendar.set(Calendar.HOUR_OF_DAY, 0)
  calendar.set(Calendar.MINUTE, 0)
  calendar.set(Calendar.SECOND, 0)
  calendar.set(Calendar.MILLISECOND, 0)
  return calendar.time
}

fun getCalendarAtGmt() = Calendar.getInstance(TimeZone.getTimeZone("GMT"))

fun Date.getDayComparator(timeZone: TimeZone = TimeZone.getDefault()): Comparator<Date> {
  return Comparator { date1, date2 -> date1.truncateToDay(timeZone).compareTo(date2.truncateToDay(timeZone)) }
}

fun Date.dateYear(): Int{
  val calendar = Calendar.getInstance()
  calendar.time = this
  return calendar.get(Calendar.YEAR)
}

fun Calendar.year(): Int {
  return this.get(Calendar.YEAR)
}

fun Calendar.month(): Int {
  return this.get(Calendar.MONTH)
}

fun Calendar.date(): Int {
  return this.get(Calendar.DATE)
}

fun Calendar.hourOfDay(): Int {
  return this.get(Calendar.HOUR_OF_DAY)
}

fun Calendar.minute(): Int {
  return this.get(Calendar.MINUTE)
}

fun Calendar.toDate(useTimeZone: TimeZone = TimeZone.getDefault()): Date {
  return time.apply { timeZone = useTimeZone }
}

fun getUserLocale(lang: Lang?): Locale {
  return when (lang) {
    null -> Locale.getDefault()
    Lang.ENG -> Locale.ENGLISH
    Lang.FRA -> Locale.FRANCE
  }
}