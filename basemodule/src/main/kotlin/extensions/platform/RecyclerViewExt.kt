package extensions.platform

/**
 * Created by artCore on 12.09.19.
 */

import android.app.Activity
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by artCore on 5/17/18.
 */
internal const val SCROLL_PIXELS_IMPER = 80

fun RecyclerView.hideKeyboardOnScroll(scrollToBottomAmount: Int = SCROLL_PIXELS_IMPER) {
    addOnScrollListener(object: RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if ((-dy) >= scrollToBottomAmount) (context as? Activity)?.hideKeyBoard()
        }
    })
}
