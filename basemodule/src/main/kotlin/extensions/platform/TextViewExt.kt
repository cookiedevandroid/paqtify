package extensions.platform

import android.view.inputmethod.EditorInfo
import android.widget.TextView

/**
 * Created by artCore on 4/16/19.
 */
inline fun TextView.doOnEditorActionPerformed(actionId: Int = EditorInfo.IME_ACTION_DONE, crossinline callback: () -> Unit) {
    setOnEditorActionListener { _, _actionId, _ ->
        if (_actionId == actionId) {
            callback()
            true
        } else {
            false
        }
    }
}