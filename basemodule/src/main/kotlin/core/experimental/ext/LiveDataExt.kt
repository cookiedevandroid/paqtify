package core.experimental.ext

/**
 * Created by artCore on 7/13/18.
 */
//--------------------------------
// CHECK THE COMMENTS FOR UPDATES!
//--------------------------------

/*
 * Copyright (C) 2017 Mitchell Skaggs, Keturah Gadson, Ethan Holtgrieve, Nathan Skelton, Pattonville School District
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import core.experimental.Resource

/**
 * This function creates a [LiveData] of a [Pair] of the two types provided. The resulting LiveData is updated whenever either input LiveData updates and both LiveData have updated at least once before.
 *
 * If the zip of A and B is C, and A and B are updated in this pattern: `AABA`, C would be updated twice (once with the second A value and first B value, and once with the third A value and first B value).
 *
 * @param a the first LiveData
 * @param b the second LiveData
 * @author Mitchell Skaggs
 */
fun <A, B> zipLiveData(a: LiveData<A>, b: LiveData<B>): LiveData<Pair<A, B>> {
  return MediatorLiveData<Pair<A, B>>().apply {
    var lastA: A? = null
    var lastB: B? = null

    fun update() {
      val localLastA = lastA
      val localLastB = lastB
      if (localLastA != null && localLastB != null)
        this.value = Pair(localLastA, localLastB)
    }

    addSource(a) {
      lastA = it
      update()
    }
    addSource(b) {
      lastB = it
      update()
    }
  }
}

fun <A> zipLiveData(a: LiveData<A>, b: LiveData<A>, func: (A) -> Unit): LiveData<Pair<A, A>> {
  return MediatorLiveData<Pair<A, A>>().apply {
    var lastA: A? = null
    var lastB: A? = null

    fun update() {
      val localLastA = lastA
      val localLastB = lastB
      if (localLastA != null && localLastB != null)
        this.value = Pair(localLastA, localLastB)
    }

    addSource(a) {
      lastA = it
      update()
    }
    addSource(b) {
      lastB = it
      update()
    }
  }
}

fun <A, B> zipLiveDataOnce(a: LiveData<A>, b: LiveData<B>): LiveData<Pair<A, B>> {
  return MediatorLiveData<Pair<A, B>>().apply {
    var lastA: A? = null
    var lastB: B? = null

    fun update() {
      val localLastA = lastA
      val localLastB = lastB
      if (localLastA != null && localLastB != null) {
        this.value = Pair(localLastA, localLastB)
        removeSource(a)
        removeSource(b)
      }
    }

    addSource(a) {
      lastA = it
      update()
    }
    addSource(b) {
      lastB = it
      update()
    }
  }
}

/**
 * This is merely an extension function for [zipLiveData].
 *
 * @see zipLiveData
 * @author Mitchell Skaggs
 */
fun <A, B> LiveData<A>.zip(b: LiveData<B>): LiveData<Pair<A, B>> =
  zipLiveData(this, b)

/**
 * This is an extension function that calls to [Transformations.map].
 *
 * @see Transformations.map
 * @author Mitchell Skaggs
 */
fun <A, B> LiveData<A>.map(function: (A) -> B): LiveData<B> = Transformations.map(this, function)

/**
 * This is an extension function that calls to [Transformations.switchMap].
 *
 * @see Transformations.switchMap
 * @author Mitchell Skaggs
 */
fun <A, B> LiveData<A>.switchMap(function: (A) -> LiveData<B>): LiveData<B> = Transformations.switchMap(this, function)

/**
 * There is some problem with Observers "onChange" func. Param in there is nullable, I don't know why.
 * So I just cast it unsafe for now. Will watch on it for some time.
 *
 *  * @author artCore
 */
inline fun <T> Fragment.observeResource(liveData: LiveData<T>, crossinline func: T.() -> T) {
  liveData.observe(this, Observer { func(it as T) })
}

/**
 * There is some problem with Observers "onChange" func. Param in there is nullable, I don't know why.
 * So I just cast it unsafe for now. Will watch on it for some time.
 *
 * @author artCore
 */
inline fun <T> AppCompatActivity.observeResource(liveData: LiveData<T>, crossinline func: T.() -> Unit) {
  liveData.observe(this, Observer { func(it as T) })
}


//fun <A> LiveData<A>.toPublisher(owner: LifecycleOwner) = LiveDataReactiveStreams.toPublisher(owner, this)
//fun <A> Publisher<A>.toLiveData() = LiveDataReactiveStreams.fromPublisher( this)
fun <A> LiveData<A>.switchMapOnSelf(): LiveData<A> =
  Transformations.switchMap(this) {
    liveDataFrom(it)
  }

fun <A, B> LiveData<A>.transform(functuion: (A?) -> B): LiveData<B> =
  Transformations.switchMap(this) {
    liveDataFrom(functuion(this.value))
  }

fun <T> liveDataFrom(source: T): LiveData<T> {
  return MutableLiveData<T>().apply { value = source }
}

fun <T> mutableLiveDataFrom(source: T): MutableLiveData<T> {
  return MutableLiveData<T>().apply { value = source }
}

@Deprecated("Should rethink usage of this extension. Funcion is not respect lifecycle and could lead to crash, I suppose")
fun <T> LiveData<T>.observeOnce(observer: Observer<T>) {
  observeForever(object : Observer<T> {
    override fun onChanged(t: T?) {
      observer.onChanged(t)
      removeObserver(this)
    }
  })
}

fun <T> LiveData<T>.observeOnceRespectState(lifecycleOwner: LifecycleOwner, observer: Observer<T>) {
  observeForever(object : Observer<T> {
    override fun onChanged(t: T?) {
      if (lifecycleOwner.lifecycle.currentState.isAtLeast(Lifecycle.State.CREATED)) {
        observer.onChanged(t)
        removeObserver(this)
      }
    }
  })
}

fun <R, LiveData : MutableLiveData<Resource<R>>> LiveData.setLoading() {
  this.postValue(Resource.loading())
}
