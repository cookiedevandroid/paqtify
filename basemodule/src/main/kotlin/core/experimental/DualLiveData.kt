package core.experimental

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import core.experimental.ext.switchMapOnSelf

/**
 * Created by artCore on 3/18/19.
 */

/**
 * This class represents the idea of having MutableLiveData inside some class (e.g. ViewModel) and expose mutableLiveData reference as
 * public immutable reference of LiveData. For general cases it is important to have exactly the
 * same reference whenever client request it. And client couldn't modify our liveData contents.
 * I have found it kind of boilerplate create inside viewModel 2 same references. 1 private and mutable
 * another one public and immutable.
 *
 * Example of usage
 * class UserViewModel : ViewModel () {
 *  ...
 *      private val user by lazy { DualLiveData<Resource<User>> } // MutableLiveData
 *
 *      init {
 *          ...
 *      }
 *
 *      fun getUser() : LiveData<Resource<User>> {
 *          user.asLiveData()
 *      }
 *
 *    // other details omitted
 * }
 *
 * */
class DualLiveData<T> : MutableLiveData<T>() {
    private val _immutable by lazy { switchMapOnSelf() }

    fun asLiveData(): LiveData<T> { return _immutable }
}
