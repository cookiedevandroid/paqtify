package core.experimental

import core.exception.Failure

/**
 * Created by artCore on 3/19/19.
 */
interface DefaultErrorHandler {
    fun handleError(t: Throwable?)
    fun handleFailure(failure: Failure)
    fun <T> handleError(error: Error<T>)
}