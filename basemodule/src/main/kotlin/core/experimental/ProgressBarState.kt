package core.experimental

/**
 * Created by artCore on 3/15/19.
 */
class ProgressBarState(private val widgetHolder: WidgetHolder) {

    interface WidgetHolder {
        fun showProgress(requestId: Int? = null)
        fun hideProgress(requestId: Int? = null)
    }

    fun <T> update(resource: Resource<T>) {
        with(resource) {
            onLoadStarted { widgetHolder.showProgress() }
            onLoadFinished { widgetHolder.hideProgress() }
        }
    }

    fun <T> update(resource: () -> Resource<T>) {
        with(resource()) {
            onLoadStarted { widgetHolder.showProgress() }
            onLoadFinished { widgetHolder.hideProgress() }
        }
    }
}



// Example of usage

/*
in Activity of Fragment
. . .
. . .

val holder = object : ProgressBarState_.WidgetHolder {
    override fun showProgress(requestId: Int?) {
        this@DeliveryDateActivity.showProgress()
    }

    override fun hideProgress(requestId: Int?) {
        this@DeliveryDateActivity.hideProgress()
    }
}

val progressState = ProgressBarState_(holder)

viewModel.updateRequest.observe {
      progressState.updateWith(this)
      onError { showErrorMessage("Server error. Details: ${it?.msg?.toString()}") }
      onSuccess {
        val intent = Intent().putExtra(DELIVERY_RESULTED_BUNDLE, data?.second)
        setResult(Activity.RESULT_OK, intent)
        finish()
      }
    }
*/
