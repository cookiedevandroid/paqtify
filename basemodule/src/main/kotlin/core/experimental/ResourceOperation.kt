package core.experimental

import androidx.lifecycle.MutableLiveData
import network.core.experimental.RetrofitNetworkError
import core.experimental.ext.setLoading
import io.reactivex.Single
import io.reactivex.disposables.Disposable

/**
 * Created by artCore on 3/15/19.
 */
class ResourceOperation<T>(
    private val liveData: MutableLiveData<Resource<T>>,
    private val loadingOnCreate: Boolean = true
) {
    init {
        if (loadingOnCreate) liveData.setLoading()
    }

    fun execute(function: () -> Single<T>): Disposable {
        with(liveData.value) {
            if (this !is Loading) {
                liveData.setLoading()
            }
        }

        return function()
            .subscribe({ res -> liveData.postValue(Resource.success(res))}, {
                it.printStackTrace()
                liveData.postValue(Resource.error(it, details = if (it is RetrofitNetworkError.Http) it.details else null))
            })
    }
}


/*
* Example of usage
*
* fun saveResults(offerId: Int) {
        val dates = deliveryDates.value
        require(dates != null)
        val request = UpdateOfferRequest.createFrom(dates)

        subscriptions.add(
            ResourceOperation(liveDataResource).execute {
                httpService.updateOffer(offerId, request).map { it to dates }
            }
        )
    }
*
* */
