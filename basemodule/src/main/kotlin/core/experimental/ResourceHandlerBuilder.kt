package core.experimental

import android.view.View
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import extensions.platform.lock
import extensions.platform.unlock
import java.util.*

class ResourceHandlerBuilder<Type>(
    private val liveData: LiveData<Resource<Type>>,
    val view: LiveDataResourceReadyView
) :
    LifecycleObserver {
    enum class Strategy {
        REPLACE, COMBINE
    }

    private val errorHandlerDefault by lazy { ResourceErrorHandler.create<Type>(view) }
    private var liveDataObserver: Observer<Resource<Type>>? = null
    private var strategy = Strategy.COMBINE
    private var handleErrors = true
    private var manageProgress = true
    private var autoLockView: Array<out View>? = null
    private val errorHandlers: MutableList<ResourceErrorHandler<Type>> by lazy { LinkedList<ResourceErrorHandler<Type>>() }

    fun autoProgress(show: Boolean = true) {
        manageProgress = show
    }

    fun autoLock(vararg views: View) {
        autoLockView = views
    }

    fun errorHandlingStrategy(strategy: Strategy) {
        this.strategy = strategy
    }

    // TODO here we should specify default error handler. And should it be replaced with the specified ? Better to have some option to switch from case to case ?
    fun addErrorHandler(handler: ResourceErrorHandler<Type>) {
        errorHandlers.add(handler)
    }

    fun addErrorHandler(handler: (err: Error<Type>) -> Unit) {
        errorHandlers.add(ResourceErrorHandler.create(handler))
    }

    fun hadleErrorByDefault(boolean: Boolean) {
        handleErrors = boolean
    }

    private fun observe(action: Resource<Type>.() -> Unit): Observer<Resource<Type>> {
        val resourceObserver = Observer<Resource<Type>> { res ->
            res?.action()
            res?.run { manageResourceHandle(this) }
        }

        liveDataObserver = resourceObserver
        liveData.observe(view, resourceObserver)
        return resourceObserver
    }

    fun onSuccess(action: (Type) -> Unit): Observer<Resource<Type>> {
        return observe {
            onSuccess(action)
        }
    }

    private fun manageResourceHandle(resource: Resource<Type>) {
        with(resource) {
            if (manageProgress) {
                onLoadStarted { view.showProgress() }
                onLoadFinished { view.hideProgress() }
            }

            if (autoLockView != null) {
                onLoadStarted { autoLockView?.forEach { it.lock()} }
                onLoadFinished { autoLockView?.forEach { it.unlock()} }
            }

            if (handleErrors) {
                onError { e ->
                    when (strategy) {
                        Strategy.COMBINE -> {
                            if (!errorHandlers.contains(errorHandlerDefault)) {
                                errorHandlers.add(errorHandlerDefault)
                            }
                            errorHandlers.forEach { it.onError(e) }
                        }
                        Strategy.REPLACE -> {
                            errorHandlers.remove(errorHandlerDefault)
                            (errorHandlers.lastOrNull() ?: errorHandlerDefault).onError(e)
                        }
                    }
                }
            }
        }
    }

    // TODO should be tested carefully
    fun dispose() {
        liveDataObserver?.let { liveData.removeObserver(it) } ?: liveData.removeObservers(view)
    }
}