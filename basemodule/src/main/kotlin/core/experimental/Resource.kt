package core.experimental

import network.core.experimental.ErrorDetails
import network.core.experimental.RetrofitNetworkError
import java.io.Serializable
import java.util.*

/**
 * Created by artCore on 3/15/19.
 */
@Deprecated("""
    this approach looks not healthy enough and lack of scalability. 
    With some corner cases data will be erased with error (if onError called all observers that started observe
    right after onError called, will get no data. But data could be loaded previously and already displayed in some view.
    """)
sealed class Resource<T> {
    companion object {
        fun <V> success(value: V) : Resource<V> = Success(value)
        fun <T> error(
            throwable: Throwable? = null,
            code: Int? = null,
            msg: CharSequence? = throwable?.message,
            id: String = UUID.randomUUID().toString(),
            details: ErrorDetails? = null
        ) : Error<T> {
            val evaluatedDetails = if (details == null && throwable is RetrofitNetworkError.Http) throwable.details else details
            return Error(throwable, code, msg, id, evaluatedDetails)
        }

        fun <T> error(throwable: Throwable?): Error<T> {
            return error(throwable, details = if (throwable is RetrofitNetworkError.Http) throwable.details else null)
        }

        // TODO if some data needed it's not a problem. "Loading" Could be extended
        fun <T> loading(requestId: Int? = null) : Resource<T> = Loading(requestId)

    }

    fun onError(onError: (e: Error<T>) -> Unit) : Resource<T> {
        if (this is Error) {
            onError(this)
        }

        return this
    }

    fun onSuccess(call: (T) -> Unit) : Resource<T> {
        @Suppress("UNCHECKED_CAST")
        when(this) {
            is Success<*> -> call(data as T)
        }
        return this
    }

    fun onLoadStarted(call: (Loading<T>) -> Unit) {
        when (this) {
            is Loading -> call(this)
        }
    }

    fun onLoadFinished(call: () -> Unit) {
        when (this) {
            !is Loading -> call()
        }
    }
}
class Loading<T>(val requestId: Int?) : Resource<T>()

// TODO maybe it should be immutable ??? Should be setData deleted ???
class Success<T>(val data: T? = null) : Resource<T>()

class Error<T>(
    val throwable: Throwable? = null,
    val code: Int? = null,
    val msg: CharSequence? = throwable?.message,
    val id: String = UUID.randomUUID().toString(),
    val details: ErrorDetails?
) : Resource<T>(), Serializable {

    override fun toString(): String {
        return "Error(throwable=$throwable, code=$code, msg=$msg, id='$id', details=$details)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Error<*>) return false

        if (throwable != other.throwable) return false
        if (code != other.code) return false
        if (msg != other.msg) return false
        if (id != other.id) return false
        if (details != other.details) return false

        return true
    }

    override fun hashCode(): Int {
        var result = throwable?.hashCode() ?: 0
        result = 31 * result + (code ?: 0)
        result = 31 * result + (msg?.hashCode() ?: 0)
        result = 31 * result + id.hashCode()
        result = 31 * result + (details?.hashCode() ?: 0)
        return result
    }

}

fun <T>Resource<T>.asData() : T? = (this as? Success<T>)?.data
