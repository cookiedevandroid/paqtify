package core.experimental

import androidx.lifecycle.LiveData

/**
 * Created by artCore on 20.08.19.
 */
// TODO test it!
class ImmutableLiveData<T>(private val initialValue: T): LiveData<T>() {

    override fun getValue(): T = initialValue
}
