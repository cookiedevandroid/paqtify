package core.experimental

interface ResourceErrorHandler<T> {
    companion object {
        fun <T> create(handler: DefaultErrorHandler): ResourceErrorHandler<T> =
            ResourceErrorHandlerImpl(handler)

        fun <T> create(handler: (err: Error<T>) -> Unit): ResourceErrorHandler<T> =
            ResourceErrorHandlerFunction(handler)
    }

    fun onError(e: Error<T>)
}

internal class ResourceErrorHandlerImpl<T>(
    private val handler: DefaultErrorHandler
) : ResourceErrorHandler<T> {
    override fun onError(e: Error<T>) = handler.handleError(e)
}

internal class ResourceErrorHandlerFunction<T>(private inline val handler: (err: Error<T>) -> Unit) :
    ResourceErrorHandler<T> {
    override fun onError(e: Error<T>) = handler(e)
}