package core.experimental

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData

/**
 * Created by artCore on 3/19/19.
 */
interface LiveDataResourceReadyView : LifecycleOwner, ProgressBarHolder, DefaultErrorHandler {

    /* Experimental stuff.*/
    fun <Type, View : LiveDataResourceReadyView> View.observeResource(
        liveData: LiveData<Resource<Type>>,
        init: ResourceHandlerBuilder<Type>.() -> Unit
    ): ResourceHandlerBuilder<Type> {
        return ResourceHandlerBuilder(liveData, this).apply(init)
    }

    fun <R, View : LiveDataResourceReadyView, LD : LiveData<Resource<R>>> LD.observeResource(
        hostView: View,
        init: ResourceHandlerBuilder<R>.() -> Unit
    ) {
        hostView.observeResource(this, init)
    }
}
