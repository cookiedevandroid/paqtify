package core.platform

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import extensions.rx.plus
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.subjects.PublishSubject

/**
 * Created by artCore on 4/5/19.
 */
abstract class BaseCoreFragment: Fragment() {
    protected val disposables = CompositeDisposable()
    private val viewCreationPublisher = PublishSubject.create<Boolean>()
    private var viewCreationDisposable: Disposable? = null
    abstract val layoutRes: Int
    open val TAG: String by lazy { simpleName() }

    protected fun simpleName(): String = this.javaClass.simpleName

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = if (layoutRes <= 0) null else inflater.inflate(layoutRes, container, false)

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    fun autoDisposable(disposableFun: () -> Disposable) {
        disposables + disposableFun()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (viewCreationPublisher.hasObservers()) {
            viewCreationPublisher.onNext(true)
        }
    }

    protected fun waitForViewCreation(func: () -> Unit) {
        if (view != null) {
            func.invoke()
        } else {
            viewCreationDisposable = viewCreationPublisher.take(1).subscribe { func.invoke() }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposeViewPublisher()
    }

    private fun disposeViewPublisher() {
        viewCreationDisposable?.dispose()
        viewCreationDisposable = null
    }

    open fun onBackPressed() {
        // noop
    }

    protected fun <T> LiveData<T>.observe(action: (T) -> Unit) {
        this.observe(this@BaseCoreFragment, Observer {
            action(it)
        })
    }

//    protected fun <T> LiveData<Resource<T>>.observeResource(action: Resource<T>.() -> Unit) {
//        this.observe(this@BaseCoreFragment, Observer {
//            it?.action()
//        })
//    }


//    override fun showProgress(requestId: Int?) {
//        activity?.runOnUiThread {
//            view?.findViewById<View>(R.id.progressBar)?.showAnim()
//        }
//    }
//
//    override fun hideProgress(requestId: Int?) {
//        activity?.runOnUiThread {
//            view?.findViewById<View>(R.id.progressBar)?.hideAnim()
//        }
//    }

//    override fun manageDisposable(disposable: Disposable?) {
//        disposable?.let { disposables.add(it) }
//    }
//
//    override fun showErrorMessage(msg: CharSequence) {
//        (activity as? BaseMVPView)?.showErrorMessage(msg)
//    }
//
//    override fun showSimpleDialog(msg: CharSequence) {
//        activity?.alert {
//            message(msg.toString())
//            okButton { }
//        }
//    }
//
//    override fun showNotImplementedYetMessage() {
//        activity?.applicationContext?.toastNotImplemented()
//    }
//
//    override fun stringFromResources(resId: Int): String = getString(resId)
//
//    override fun stringFromResourcesWithArgs(resId: Int, args: Array<Int>): String {
//        val strings = if (args.isEmpty()) "" else args.joinToString { resources.getString(it) }
//        return resources.getString(resId, strings)
//    }
//
//    override fun showErrorMessage(resId: Int) = showErrorMessage(getString(resId))

}
