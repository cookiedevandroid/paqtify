package core.platform

import androidx.lifecycle.ViewModel
import extensions.rx.plus
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.exceptions.UndeliverableException

/**
 * Created by artCore on 7/13/18.
 */
abstract class BaseCoreViewModel : ViewModel() {
    private val disposables: CompositeDisposable by lazy { CompositeDisposable() }

    override fun onCleared() {
        super.onCleared()
        try {
            disposables.clear()
        } catch (e: UndeliverableException) {
            e.printStackTrace()
        }

    }

    fun bindDisposable(disposable: Disposable) {
        disposables + disposable
    }

    fun autoDisposable(disposableFun: () -> Disposable) {
        disposables + disposableFun()
    }
}


