package core.platform

import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by artCore on 4/5/19.
 */
abstract class BaseCoreActivity: AppCompatActivity() {
    protected val disposables: CompositeDisposable by lazy { CompositeDisposable() }

    protected fun makeFullScreen() {
//        must called before set content
//        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
            WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
        )

        window.setFlags(
            WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION,
            WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION
        )
    }

    protected fun outOfFullScreen() {
        window.clearFlags(
            WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
        )

        window.clearFlags(
            WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION
        )
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    protected fun <T> LiveData<T>.observe(action: (T) -> Unit) {
        this.observe(this@BaseCoreActivity, Observer {
            action(it)
        })
    }
}
